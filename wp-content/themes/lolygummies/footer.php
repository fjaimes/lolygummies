<footer class="bg-pink">
    <div class="container py-5">
        <div class="row">
            <div class="col-12 mb-5">
                <div class="d-flex justify-content-center align-items-center gap-4">
                    <?php
                        $text = get_theme_mod('footer_text_top');
                        $text = explode(' ', $text);
                        $middle = floor(count($text) / 2);

                        $first_half = array_slice($text, 0, $middle);
                        $second_half = array_slice($text, $middle);
                    ?>

                    <img src="<?= esc_url(wp_get_attachment_url(get_theme_mod('footer_logo'))) ?>" alt="Logo LolyGummies" class="img-fluid logo" height="58" width="57">
                    <h3 class="m-0"><?= join(' ', $first_half) ?> <strong><?= join(' ', $second_half) ?? "" ?></strong></h3>
                </div>
                <ul class="social">
                    <?php if(!empty(get_theme_mod('facebook'))): ?>
                        <li>
                            <a target="_blank" rel="noopener" href="<?= get_theme_mod('facebook') ?>">
                                <span class="screen-reader-text">facebook</span>
                                <i class="fa-brands fa-facebook"></i>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if(!empty(get_theme_mod('instagram'))): ?>
                        <li>
                            <a target="_blank" rel="noopener" href="<?= get_theme_mod('instagram') ?>">
                                <span class="screen-reader-text">instagram</span>
                                <i class="fa-brands fa-instagram"></i>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if(!empty(get_theme_mod('tiktok'))): ?>
                        <li>
                            <a target="_blank" rel="noopener" href="<?= get_theme_mod('tiktok') ?>">
                                <span class="screen-reader-text">tiktok</span>
                                <i class="fa-brands fa-tiktok"></i>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if(!empty(get_theme_mod('twitter'))): ?>
                        <li>
                            <a target="_blank" rel="noopener" href="<?= get_theme_mod('twitter') ?>">
                                <span class="screen-reader-text">twitter</span>
                                <i class="fa-brands fa-twitter"></i>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if(!empty(get_theme_mod('youtube'))): ?>
                        <li>
                            <a target="_blank" rel="noopener" href="<?= get_theme_mod('youtube') ?>">
                                <span class="screen-reader-text">youtube</span>
                                <i class="fa-brands fa-youtube"></i>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if(!empty(get_theme_mod('pinterest'))): ?>
                        <li>
                            <a target="_blank" rel="noopener" href="<?= get_theme_mod('pinterest') ?>">
                                <span class="screen-reader-text">pinterest</span>
                                <i class="fa-brands fa-pinterest"></i>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-3">
                <h4>CONTACTO</h4>
                <p>
                    <?= nl2br(get_theme_mod('direccion')); ?>
                </p>
                <p>
                    MAIL:
                    <a href="mailto:<?= get_theme_mod('email') ?>">
                        <?= get_theme_mod('email') ?>
                    </a>
                </p>
                <p>
                    TEL:
                    <a href="tel:<?= str_replace([" ", "(", ")"], "", get_theme_mod('tel_number')) ?>">
                        <?= get_theme_mod('tel_number') ?>
                    </a>
                </p>
            </div>
            <div class="col-md-3">
                <h4>AYUDA Y SOPORTE</h4>
                <?php
                wp_nav_menu([
                    'theme_location' => 'footer_ayuda',
                    'menu_class' => 'navbar-nav',
                    'container' => false
                ]);
                ?>
            </div>
            <div class="col-md-3">
                <h4>ACERCA DE NOSOTROS</h4>
                <?php
                wp_nav_menu([
                    'theme_location' => 'footer_nosotros',
                    'menu_class' => 'navbar-nav',
                    'container' => false
                ]);
                ?>
            </div>
            <div class="col-md-3">
                <h4>PRODUCTOS</h4>
                <?php
                wp_nav_menu([
                    'theme_location' => 'footer_productos',
                    'menu_class' => 'navbar-nav',
                    'container' => false
                ]);
                ?>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12">
                <div class="d-flex justify-content-center gap-3">
                    <a href="/aviso-privacidad/" style="text-decoration: underline;">Aviso de Privacidad</a>
                    <a href="mailto:<?= get_theme_mod('email_contacto') ?>">
                        <?= get_theme_mod('email_contacto') ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="whatsapp">
    <a href="https://api.whatsapp.com/send?phone=<?= str_replace([" ", "(", ")"], "", get_theme_mod('whatsapp')) ?>" target="_blank">
    <img src="<?php echo get_template_directory_uri(); ?>/image/whatsapp.svg" alt="Whatsapp" width="78" height="79" />
    </a>
</div>

<?php wp_footer(); ?>
</body>
</html>
