<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo('name'); ?></title>

    <?php wp_head(); ?>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>">
</head>
<body>
    <div class="header bg-pink-secondary">
        <div class="container">
            <nav class=" d-flex align-items-center">
                <div class="mx-auto text-envio">
                    <img src="<?php echo get_template_directory_uri(); ?>/image/box.svg" alt="Envíos a toda la República Mexicana" width="36" height="36"> <?= get_theme_mod('text_top') ?>
                </div>

                <ul id="social">
                    <?php if(!empty(get_theme_mod('facebook'))): ?>
                    <li>
                        <a target="_blank" rel="noopener" href="<?= get_theme_mod('facebook') ?>">
                            <span class="screen-reader-text">facebook</span>
                            <i class="fa-brands fa-facebook"></i>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php if(!empty(get_theme_mod('instagram'))): ?>
                    <li>
                        <a target="_blank" rel="noopener" href="<?= get_theme_mod('instagram') ?>">
                            <span class="screen-reader-text">instagram</span>
                            <i class="fa-brands fa-instagram"></i>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php if(!empty(get_theme_mod('tiktok'))): ?>
                    <li>
                        <a target="_blank" rel="noopener" href="<?= get_theme_mod('tiktok') ?>">
                            <span class="screen-reader-text">tiktok</span>
                            <i class="fa-brands fa-tiktok"></i>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php if(!empty(get_theme_mod('twitter'))): ?>
                    <li>
                        <a target="_blank" rel="noopener" href="<?= get_theme_mod('twitter') ?>">
                            <span class="screen-reader-text">twitter</span>
                            <i class="fa-brands fa-twitter"></i>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php if(!empty(get_theme_mod('youtube'))): ?>
                    <li>
                        <a target="_blank" rel="noopener" href="<?= get_theme_mod('youtube') ?>">
                            <span class="screen-reader-text">youtube</span>
                            <i class="fa-brands fa-youtube"></i>
                        </a>
                    </li>
                    <?php endif; ?>

                    <?php if(!empty(get_theme_mod('pinterest'))): ?>
                    <li>
                        <a target="_blank" rel="noopener" href="<?= get_theme_mod('pinterest') ?>">
                            <span class="screen-reader-text">pinterest</span>
                            <i class="fa-brands fa-pinterest"></i>
                        </a>
                    </li>
                    <?php endif; ?>
                </ul>
            </nav>
        </div>
    </div>

    <nav class="navbar navbar-expand-md sticky-top bg-white pt-4 pb-3">
        <div class="container">
            <div class="d-flex flex-wrap justify-content-between align-items-center w-100">
                <?php the_custom_logo(); ?>

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#menu-principal" aria-controls="menu-principal" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse mx-auto" id="menu-principal">
                    <?php
                    wp_nav_menu([
                        'theme_location' => 'navegation',
                        'menu_class' => 'navbar-nav mx-auto',
                        'container' => false
                    ]);
                    ?>
                    <?php
                    wp_nav_menu([
                        'theme_location' => 'user',
                        'menu_class' => 'navbar-nav',
                        'container' => false
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </nav>

