<?php

namespace LolyGummies\Classes;
use WP_Customize_Control;

class IconTextControl extends WP_Customize_Control
{
    public $type = 'icon_text';
    public $icon = '';

    public function render_content(): void
    {
        $html = <<<HTML
            <label class="customize-control-title">{$this->label}</label>
            <div style="display: flex; align-items: center; justify-content: center;">
                <i class="{$this->icon}" style="font-size: 28px; width: 32px; height: 28px;"></i>        
                <input type="text" {$this->get_link()} value="{$this->value()}">
            </div>
        HTML;

        echo $html;
    }
}
