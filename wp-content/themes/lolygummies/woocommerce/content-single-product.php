<?php

/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action('woocommerce_before_single_product');

if (post_password_required()) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}

$_bellaza_y_salud = get_field('bellaza_y_salud');
$_banner = get_field('banner');
$_mas_informacion = get_field('mas_informacion');
$_testimonios = get_field('testimonios');

?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class('py-5', $product); ?>>
	<div class="row g-5">
		<div class="col-lg-5">
			<?php
			/**
			 * Hook: woocommerce_before_single_product_summary.
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10
			 * @hooked woocommerce_show_product_images - 20
			 */
			do_action('woocommerce_before_single_product_summary');
			?>
		</div>
		<div class="col-lg-7">
			<?php
			/**
			 * Hook: woocommerce_single_product_summary.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_price - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 * @hooked WC_Structured_Data::generate_product_data() - 60
			 */
			do_action('woocommerce_single_product_summary');
			?>
			<ul class="info">
				<li><span>Envío gratis</span> en compras mayores a $599</li>
				<li>Tu compra está protegida</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<hr>
			<?php
			/**
			 * Hook: woocommerce_after_single_product_summary.
			 *
			 * @hooked woocommerce_output_related_products - 10
			 * @hooked woocommerce_output_product_data_tabs - 15
			 * @hooked woocommerce_upsell_display - 20
			 */
			do_action('woocommerce_after_single_product_summary');
			?>
			<?php if($_bellaza_y_salud): ?>
			<div class="card card_shadow2 card_belleza_salud my-5">
				<div class="card-body">
					<div class="row">
						<div class="col-lg-7">
							<h6><?= $_bellaza_y_salud["titulo"] ?></h6>
							<h2><?= $_bellaza_y_salud["subtitulo"] ?></h2>
							<p><?= $_bellaza_y_salud["descripcion"] ?></p>
							<div class="accordion accordion-flush" id="accOpcion">
								<?php
								$opciones = str_replace(["<ul>", "</ul>"], "", $_bellaza_y_salud["opciones"]);

								$opciones = explode("<li>", $opciones);

								foreach ($opciones as $cve => $opcion) {
									$opcion = trim($opcion);

									if (!empty($opcion)) {
										$opt = explode("<pre>", $opcion);
									?>
										<div class="accordion-item">
											<h2 class="accordion-header">
												<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#acc_<?= $cve ?>" aria-expanded="false" aria-controls="acc_<?= $cve ?>">
													<?= $opt[0] ?>
												</button>
											</h2>
											<div id="acc_<?= $cve ?>" class="accordion-collapse collapse" data-bs-parent="#accOpcion">
												<div class="accordion-body">
													<?= trim(str_replace(["<pre>", "</pre>"], "", $opt[1])) ?>
												</div>
											</div>

										</div>
										

									<?php
									}
								}
								?>
							</div>
						</div>
						<div class="col-lg-5 text-center">
							<img src="<?= $_bellaza_y_salud['imagen'] ?>" alt="<?= $_bellaza_y_salud["titulo"] ?>" class="img-fluid" />
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<h6>Instrucciones de uso:</h6>
							<p>
								<?= $_bellaza_y_salud["instrucciones"] ?>
							</p>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php if($_banner): ?>
<div class="over-position banner">
	<img src="<?= $_banner ?>" alt="banner" class="img-fluid" />
</div>
<?php endif; ?>
<?php if($_mas_informacion): ?>
<div class="container-fluid over-position mas-informacion py-5">
	<div class="container pt-5">
		<div class="row">
			<div class="col-lg-5">
			 	<img src="<?= $_mas_informacion["informacion_1"]["imagen"] ?>" alt="<?= $_mas_informacion["informacion_1"]["titulo"] ?>" width="470"  />
			</div>
			<div class="col-lg-7">
				<h2><?= $_mas_informacion["informacion_1"]["titulo"] ?></h2>
				<?= $_mas_informacion["informacion_1"]["descripcion"] ?>
			</div>
		</div>
		<div class="row py-3">
			<div class="col-12">
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-7">
				<img src="<?php echo get_template_directory_uri(); ?>/image/people.png" alt="nuestro compromiso con tu bienestar" width="203" height="197" style="float:left; margin-right: 25px;">
				<h3><?= $_mas_informacion["informacion_2"]["titulo"] ?></h3>
				<?= $_mas_informacion["informacion_2"]["descripcion"] ?>
			</div>
			<div class="col-lg-5 text-center">
				<img src="<?= $_mas_informacion["informacion_2"]["imagen"] ?>" alt="<?= $_mas_informacion["informacion_2"]["titulo"] ?>" width="360"  />
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php if($_testimonios): ?>
<div class="container testimonio py-5">
	<div class="row">
		<div class="col-12">
			<h2 class="text-center">Testimonios de nuestros clientes</h2>

			<div class="card card_shadow2 my-5">
				<div class="card-body text-center">
					<img src="<?= $_testimonios["imagen"] ?>" alt="Testimonio" class="user" width="192" height="192">

					<p>
						<?= $_testimonios["testimonio"] ?>
					</p>
					<img src="<?= $_testimonios["estrellas"] ?>" alt="Testimonio" class="rank" width="130" height="22">

					<p class="nombre">
						<?= $_testimonios["nombre"]; ?>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php do_action('woocommerce_after_single_product'); ?>