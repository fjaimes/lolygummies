<?php

/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

// Ensure visibility.
if (empty($product) || !$product->is_visible()) {
	return;
}
?>
<?php
$product_title = get_the_title($product->get_id());
$product_link = get_permalink($product->get_id());
$image_url = get_the_post_thumbnail_url($product->get_id());
$categories = get_the_terms($product->get_id(), 'product_cat');
$first_category = $categories[0];
$category_name = $first_category->name;
$category_link = get_term_link($first_category);
$product_price = $product->get_price();
?>
<div class="product col-sm-6 my-3">
	<div class="card">
		<div class="card-body">
			<div class="row h-100">
				<div class="col-xl-5 col-xxl-6 text-center">
					<img width="300" height="300" src="<?= $image_url ?>" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail">
				</div>
				<div class="col-xl-7 col-xxl-6">
					<div class="d-flex justify-content-between flex-column h-100">
						<h4>
							<a href="<?= $product_link ?>" aria-label="' . $product_title . '"><?= $product_title ?></a>
						</h4>

						<div class="d-flex flex-column my-3">
							<?php echo do_shortcode('[stars_rating_avg id="' . get_the_ID() . ']') ?>
							<span class="price">$ <?= number_format($product_price, 2) ?></span>
							<?php
							/**
							 * Hook: woocommerce_after_shop_loop_item.
							 *
							 * @hooked woocommerce_template_loop_product_link_close - 5
							 * @hooked woocommerce_template_loop_add_to_cart - 10
							 */
							do_action('woocommerce_after_shop_loop_item');
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>