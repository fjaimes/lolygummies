<?php
/**
 * The template for displaying WooCommerce pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package LolyGummies
 * @author FJaimes
 */

get_header('shop'); ?>

<div id="primary" class="container">
    <?php woocommerce_content(); ?>

</div>
<?php
get_footer('shop');