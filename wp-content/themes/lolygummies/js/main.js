$(document).ready(function() {
    $('#slOrdering').select2({
        minimumResultsForSearch: Infinity
    });

    if($(".productos").length > 0){
        $(".sp-wqv-view-button").each(function(){
            let _hover = $(this).closest(".card").find(".hover");
            $(this).appendTo(_hover);

            _hover.height(_hover.siblings("img").height());
        });
    }

    jQuery(document.body).on('removed_from_cart updated_cart_totals', function() {
        jQuery.ajax({
            type: 'POST',
            url: wc_add_to_cart_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'get_refreshed_fragments' ),
            success: function( data ) {
                if ( data && data.fragments ) {
                    jQuery.each( data.fragments, function( key, value ) {
                        jQuery( key ).replaceWith( value );
                    });
                }
            }
        });
    });
    
});