<?php
/**
 * Functions and definitions
 *
 * @package LolysGummies
 * @author FJaimes
 * @since 1.0.0
 */

declare( strict_types=1 );

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

function add_main_nav(): void
{
    register_nav_menus(['navegation' => __('Menú de navegación')]);
    register_nav_menus(['user' => __('Menú de usuario')]);
    register_nav_menus(['footer_ayuda' => __('Menú Ayuda - pie de página')]);
    register_nav_menus(['footer_nosotros' => __('Menú Nosotros - pie de página')]);
    register_nav_menus(['footer_productos' => __('Menú Productos - pie de página')]);
}

function add_widget_support(): void
{
    register_sidebar([
        'name' => 'Sidebar',
        'id' => 'sidebar',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ]);
}

function add_jquery_support(): void
{
    wp_register_script('main_js', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.0', true);
    wp_register_script('blockui', get_template_directory_uri() . '/js/jquery.blockUI.js', array('jquery'), false, true);

    wp_enqueue_script('jquery-script', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js');
    wp_enqueue_script('main_js');
    wp_enqueue_script('blockui');
}

function add_select2_support(): void
{
    wp_enqueue_style('select2-styles', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css');
    wp_enqueue_script('select2-script', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js');
}

function add_bootstrap_support(): void
{
    wp_enqueue_style( 'bootstrap-styles', "https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css");
    wp_enqueue_script('bootstrap-script', 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js');

    wp_enqueue_script('sweetalert-script', "https://cdn.jsdelivr.net/npm/sweetalert2@11");
}

function add_fontawesome_support(): void
{
    wp_enqueue_style( 'fontawesome-styles', "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css");
    wp_enqueue_style( 'animate-styles', "https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css");
}

function lolygummies_setup(): void
{
    add_theme_support( 'woocommerce' );
    add_theme_support('post-thumbnails');
    add_theme_support('custom-logo', [
        'height' => 49,
        'width' => 149,
        'flex-height' => true,
    ]);
}

function lolygummies_login(): void
{
    echo '<style type="text/css">
        body.login { background-color: rgba(207,52,118,0.5) !important; color: #000 !important; }
        h1 a { 
            background-image: url(' .get_bloginfo('template_directory').'/image/logo.webp) !important;
            width: 100% !important;
            height: 110px !important;
            margin: 10px !important;
            background-size: 100% !important;            
         }
         
        </style>';
}

function lolygummies_footer_admin(): void
{
    echo '&copy;2023 Copyright LolyGummies. Todos los derechos reservados - Web creada por <a href="https://www.fjaimes.com">FJaimes</a>';
}

function lolygummies_change_toolbar($wp_toolbar): void
{
    $wp_toolbar->remove_node('comments');
    $wp_toolbar->remove_node('new-content');
}

function lolygummies_remove_meta_box(): void{
    remove_post_type_support( 'post', 'comments');
    remove_post_type_support( 'post', 'trackbacks');
    remove_post_type_support( 'post', 'excerpt');
    remove_post_type_support( 'post', 'revisions');

    remove_post_type_support( 'page', 'comments');
    remove_post_type_support( 'page', 'trackbacks');
    remove_post_type_support( 'page', 'excerpt');
    remove_post_type_support( 'page', 'revisions');
}

function lolygummies_toolbars($toolbars ): array
{
    $toolbars['Basic'][1][] = 'forecolor';
    $toolbars['Basic'][1][] = 'formatselect';

    return $toolbars;
}

function lolygummies_add_general_register($wp_customize): void
{
    $wp_customize->add_section('general', array(
        'title' => __('Datos generales', 'lolygummies'),
        'description' => 'Agregar datos generales',
        'priority' => 50,
    ));

    $wp_customize->add_setting('text_top', [
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field',
    ]);

    $wp_customize->add_setting('footer_text_top', [
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field',
    ]);

    $wp_customize->add_setting('footer_logo', array(
        'default' => '',
        'sanitize_callback' => 'absint',
    ));

    $wp_customize->add_setting('tel_number', [
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field',
    ]);

    $wp_customize->add_setting('email', [
        'default' => '',
        'sanitize_callback' => 'sanitize_email',
    ]);

    $wp_customize->add_setting('email_contacto', [
        'default' => '',
        'sanitize_callback' => 'sanitize_email',
    ]);

    $wp_customize->add_setting('direccion', array(
        'default'        => '',
        'sanitize_callback' => 'sanitize_textarea_field',
    ));

    $wp_customize->add_control('text_top', [
        'label'   => __('Texto top header', 'lolygummies'),
        'type' => 'text',
    ]);

    $wp_customize->add_control('footer_text_top', [
        'label'   => __('Texto pie de página', 'lolygummies'),
        'section' => 'general',
        'type' => 'text',
    ]);

    $wp_customize->add_control('tel_number', [
        'label'   => __('Número de Teléfono', 'lolygummies'),
        'section' => 'general',
        'type'    => 'text',
    ]);

    $wp_customize->add_control('email', [
        'label'   => __('Correo electrónico', 'lolygummies'),
        'section' => 'general',
        'type'    => 'email',
    ]);

    $wp_customize->add_control('email_contacto', [
        'label'   => __('Correo electrónico para contacto', 'lolygummies'),
        'section' => 'general',
        'type'    => 'email',
    ]);

    $wp_customize->add_control('direccion', array(
        'label'   => __('Domicilio', 'lolygummies'),
        'section' => 'general',
        'type'    => 'textarea',
    ));

    $wp_customize->add_control(new WP_Customize_Cropped_Image_Control($wp_customize, 'footer_logo', [
        'label'   => __('Logo del pie de página', 'lolygummies'),
        'section' => 'general',
        'flex_width'  => true,
        'flex_height' => true,
        'width'       => 200,
        'height'      => 200,
    ]));
}

function lolygummies_add_social_register($wp_customize): void
{
    require_once __DIR__ . '/classes/IconTextControl.php';

    $wp_customize->add_section('social', array(
        'title' => __('Redes sociales', 'lolygummies'),
        'description' => 'Agregar link de redes sociales',
        'priority' => 60,
    ));

    $wp_customize->add_setting('facebook', [
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field',
    ]);

    $wp_customize->add_setting('instagram', [
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field',
    ]);

    $wp_customize->add_setting('tiktok', [
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field',
    ]);

    $wp_customize->add_setting('twitter', array(
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_setting('youtube', array(
        'default'        => '',
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_setting('pinterest', array(
        'default'        => '',
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_setting('whatsapp', array(
        'default'        => '',
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(new LolyGummies\Classes\IconTextControl($wp_customize, 'facebook', [
        'label'   => __('Facebook', 'lolygummies'),
        'section' => 'social',
        'icon' => 'fa-brands fa-facebook',
    ]));

    $wp_customize->add_control(new LolyGummies\Classes\IconTextControl($wp_customize, 'instagram', [
        'label'   => __('Instagram', 'lolygummies'),
        'section' => 'social',
        'icon' => 'fa-brands fa-instagram',
    ]));

    $wp_customize->add_control(new LolyGummies\Classes\IconTextControl($wp_customize, 'tiktok', [
        'label'   => __('TikTok', 'lolygummies'),
        'section' => 'social',
        'icon' => 'fa-brands fa-tiktok',
    ]));

    $wp_customize->add_control(new LolyGummies\Classes\IconTextControl($wp_customize, 'twitter', [
        'label'   => __('Twitter', 'lolygummies'),
        'section' => 'social',
        'icon' => 'fa-brands fa-twitter',
    ]));

    $wp_customize->add_control(new LolyGummies\Classes\IconTextControl($wp_customize, 'youtube', [
        'label'   => __('Youtube', 'lolygummies'),
        'section' => 'social',
        'icon' => 'fa-brands fa-youtube',
    ]));

    $wp_customize->add_control(new LolyGummies\Classes\IconTextControl($wp_customize, 'pinterest', [
        'label'   => __('Pinterest', 'lolygummies'),
        'section' => 'social',
        'icon' => 'fa-brands fa-pinterest',
    ]));

    $wp_customize->add_control(new LolyGummies\Classes\IconTextControl($wp_customize, 'whatsapp', [
        'label'   => __('Whatsapp', 'lolygummies'),
        'section' => 'social',
        'icon' => 'fa-brands fa-whatsapp',
    ]));
}

function lolygummies_add_menu_link_class($atts, $item, $args): array
{
    if ( property_exists( $args, 'link_class' ) ) {
        $atts['class'] = $args->link_class;
    }

    return $atts;
}

function lolygummies_menu_link_class($atts, $item, $args): array
{
    if ( $args->theme_location == 'user' ) {
        if ( $item->title == 'Conviertete en Distribuidor' ) {
            $atts['class'] = 'btn btn-pink';
        }else{
            $atts['class'] = 'menu-icono';
        }
    }

    return $atts;
}

function lolygummies_menu_item_title($title, $item, $args, $depth): string
{
    if ( $args->theme_location == 'user' ) {
        if ( $item->title == 'Conviertete en Distribuidor' ) {
            $title = '<span>' . $title . '</span><i class="fa-solid fa-arrow-right-long"></i>';
        }

        if ( $item->title == 'Mi cuenta' ) {
            $title = '<img src="' . get_template_directory_uri() .'/image/user.svg" alt="Mi cuenta" width="44" height="44">';
        }

        if ( $item->title == 'Carrito' ) {
            $title = '<img src="' . get_template_directory_uri() .'/image/bag.svg" alt="Carrito de compra" width="44" height="44"><span class="badge rounded-pill text-bg-primary">'.  WC()->cart->get_cart_contents_count() . '</span>
            ';
        }
    }

    return $title;
}

function lolygummies_mime_types($mimes): array
{
    $mimes['svg'] = 'image/svg+xml';

    return $mimes;
}

function lolygummies_smtp_settings($phpmailer) {
    $phpmailer->isSMTP();
    $phpmailer->Host = 'mail.fjaimes.com';
    $phpmailer->Port = 465;
    $phpmailer->SMTPAuth = true;
    $phpmailer->Username = 'noreply@fjaimes.com';
    $phpmailer->Password = 'Lu15yN0rm2.';
    $phpmailer->SMTPSecure = 'ssl';
    $phpmailer->From = 'noreply@fjaimes.com';
    $phpmailer->FromName = 'LolyGummies';
}



add_action('init', 'add_main_nav');
add_action('init', 'lolygummies_remove_meta_box');
add_action( 'widgets_init', 'add_widget_support' );
add_action( 'wp_enqueue_scripts', 'add_jquery_support');
add_action( 'wp_enqueue_scripts', 'add_select2_support');
add_action( 'wp_enqueue_scripts', 'add_bootstrap_support');
add_action('wp_enqueue_scripts', 'add_fontawesome_support');
add_action('admin_enqueue_scripts', 'add_fontawesome_support');

add_action('customize_register', 'lolygummies_add_general_register');
add_action('customize_register', 'lolygummies_add_social_register');

add_action('after_setup_theme', 'lolygummies_setup');
add_action('login_head', 'lolygummies_login');
add_action('admin_footer_text', 'lolygummies_footer_admin');
add_action('admin_bar_menu', 'lolygummies_change_toolbar', 999);

add_action('phpmailer_init', 'lolygummies_smtp_settings');

add_filter('acf/fields/wysiwyg/toolbars', 'lolygummies_toolbars');
add_filter('show_admin_bar', '__return_false');
add_filter('nav_menu_link_attributes', 'lolygummies_add_menu_link_class', 1, 3);
add_filter('nav_menu_link_attributes', 'lolygummies_menu_link_class', 10, 3);
add_filter('nav_menu_item_title', 'lolygummies_menu_item_title', 10, 4);
add_filter('upload_mimes', 'lolygummies_mime_types');


/*** Woocommerce ***/

function lolygummies_change_woocommerce_breadcrumb_separator( $defaults ): array {
    // Cambia el separador de '/' a '>'
    $defaults['delimiter'] = '<i class="fa-solid fa-angle-right"></i>';
    return $defaults;
}

function lolygummies_before_shop_loop(): void {
    echo '<div class="row">';
    echo '<div class="col-6">';
    woocommerce_breadcrumb();
    echo '</div>';
    echo '<div class="col-6">';
    echo '<div class="d-flex justify-content-end align-items-center gap-3">';
    echo '<div class="dvResult">';
    woocommerce_result_count();
    echo '</div>';
    echo '<div class="dvOrdering">';
    woocommerce_catalog_ordering();
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
}

function lolygummies_price_and_rating() {
    echo '<div class="d-flex align-items-center my-3">';
    woocommerce_template_single_price();
    woocommerce_template_single_rating();
    echo '</div>';
}

function lolygummies_add_to_cart_fragments( $fragments ): array {
    ob_start();
    ?>
    <span class="badge rounded-pill text-bg-primary"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
    <?php
    $fragments['span.badge'] = ob_get_clean();
    return $fragments;
}

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

add_action('woocommerce_single_product_summary', 'lolygummies_price_and_rating', 10);

remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

add_filter('woocommerce_breadcrumb_defaults', 'lolygummies_change_woocommerce_breadcrumb_separator');
add_filter('woocommerce_add_to_cart_fragments', 'lolygummies_add_to_cart_fragments');

add_action('woocommerce_before_shop_loop', 'lolygummies_before_shop_loop', 20);


