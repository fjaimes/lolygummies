<?php
/**
 * Template Name: Mi Cuenta
 */
?>
<?php get_header('shop'); ?>
    <div class="container my-account py-3">
        <?php echo do_shortcode('[woocommerce_my_account]'); ?>
    </div>
<?php get_footer('shop'); ?>