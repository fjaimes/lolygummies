<?php
/**
 * Template Name: Finalizar Compra
 */
?>
<?php get_header('shop'); ?>
    <div class="container my-account py-5">
        <?php echo do_shortcode('[woocommerce_checkout]'); ?>
    </div>
<?php get_footer('shop'); ?>