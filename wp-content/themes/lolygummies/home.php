<?php
/**
 * Template Name: Home
 */
?>

<?php get_header(); ?>

<?php
if ( have_posts() ) :
    the_post();

    $_acerca_de = get_field('acerca_de');
    $_distribuidor = get_field('distribuidor');

    $terms = get_terms( 'product_cat', array(
        'hide_empty' => true,
    ));

    echo do_shortcode('[smartslider3 slider="1"]');
?>
    <div class="container-fluid bg-mask-point">
        <div class="container mb-2 info_acerca">
            <div class="row justify-content-center">
                <div class="col-md-9 card_shadow">
                    <div class="row">
                        <?php foreach ($_acerca_de as $acerca): ?>
                            <div class="col-12 col-md-3 text-center">
                                <figure class="wp-block-image size-full">
                                    <img width="113" height="110" src="<?= $acerca['icono'] ?>" alt="<?= $acerca['titulo'] ?>" class="wp-image-109">
                                </figure>
                                <p><?=  nl2br($acerca['titulo']) ?></p>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container categorias pb-4">
            <div class="col-12">
                <h2 class="text-center py-5">Nuestros productos</h2>

                <div class="d-flex justify-content-between">
                    <?php foreach ($terms as $term): ?>
                        <div class="text-center categoria">
                            <?php
                                $thumbnail_id = get_term_meta( $term->term_id, 'thumbnail_id', true );
                                $image = wp_get_attachment_url( $thumbnail_id );
                            ?>
                            <a href="<?= get_term_link($term) ?>">
                                <img src="<?= esc_url( $image ) ?>" alt="<?= esc_attr( $term->name ) ?>" width="247" height="254" class="mb-3"/>
                                <p><?= $term->name ?></p>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid bg-white py-5 distribuidor">
        <div class="container mb-2 card_shadow px-5">
            <div class="row">
                <div class="col-md-7 ps-4">
                    <div class="w-100 position-relative" style="height: 100%;">
                        <h2><?= $_distribuidor["titulo"] ?></h2>
                        <p><?= str_replace("LOLY", "<b>LOLY</b>", $_distribuidor["descripcion"]) ?></p>
                        <a href="/conviertete-en-distribuidor/"><?= $_distribuidor["texto_boton"] ?></a>
                    </div>
                </div>
                <div class="col-md-5 text-center">
                    <img width="438" height="283" src="<?= $_distribuidor['imagen'] ?>" alt="<?= $_distribuidor["titulo"] ?>">
                </div>
            </div>
            <hr>
            <div class="row g-4">
                <?php foreach($_distribuidor["cuadros_informativos"] as $cuadros_informativo): ?>
                    <div class="col-md-3 indicadores">
                        <span><?= $cuadros_informativo["numerico"] ?></span>
                        <p><?= $cuadros_informativo["descripcion"] ?></p>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="container-fluid bg-mask-slick py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>¡Realiza tu pedido aquí!</h2>

                    <p>
                        Mail:
                        <a href="mailto:<?= get_theme_mod('email_contacto') ?>">
                            <?= get_theme_mod('email_contacto') ?>
                        </a>
                    </p>
                    <p>
                        <a href="tel:<?= str_replace([" ", "(", ")"], "", get_theme_mod('tel_number')) ?>">
                            <?= get_theme_mod('tel_number') ?>
                        </a>
                    </p>

                    <a href="/tienda/" class="btnIr">COMPRAR EN LÍNEA</a>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php get_footer(); ?>
